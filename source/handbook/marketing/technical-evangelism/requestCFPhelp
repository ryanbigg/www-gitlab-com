---
layout: markdown_page
title: "Technical Evangelism: Requesting help with a CFP"
---

## On this page
{:.no_toc}

- TOC
{:toc}

----

The team is here to support individuals who want to speak on behalf of GitLab. You can check out our general speaker program on the [GitLab Evangelist Program] page(https://about.gitlab.com/community/evangelists/become-a-speaker/).

 ### Calls for Papers
If you want to be added to GitLab's [Find a speaker page] (https://about.gitlab.com/events/find-a-speaker/) for a technical topic, reach out to the Technical Evangelism team. We're excited to support you as you work on any CFPs, speaker proposals, or other means of promoting the great technical work that our team does.

In order to ensure that your request gets eyeballs, please complete the following:
1. Join the [#cfp] (https://gitlab.slack.com/messages/C106ACT6C/) Slack channel and share the event and prompt
2. Start an issue in the technical evangelism project using the CFP submissions template and tag any associated event issues
3. Complete as much info as possible and we will ping you with next steps

We are happy to help in anyway we can, including public speaking coaching and connecting you with partners inside and out of GitLab.
