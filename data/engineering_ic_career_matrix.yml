- name: Intermediate
  "Values Alignment":
    Collaboration:
    - Independently brings in expertise from other contributors to raise the standard of their work.
    - Gives positive feedback in public and negative feedback privately with care.
    Results:
    - Executes tasks in team projects, demonstrating ownership of them from conception to completion. Requires supervision to deliver projects in time, growing bias for action.
    Efficiency:
    - Recognises complexity of the team's product or the team's process, and proposes solutions accordingly.
    - Capable of implementing solutions based on the complexity, and asks for guidance when the complexity increases unexpectedly.
    - Proposes changes that improve the efficiency of the team.
    Diversity:
    Iteration:
    - Balances short term gains and long term benefit with team's help.
    - Focuses on improvement.
    Transparency:
    - Provides context and background on projects and issues so that those with no prior knowledge are able to contribute to the discussion.
  "Technical Competencies":
    Quality:
    - Expected to write tests with every MR and work to uplift the quality of the code.
    - When working in an existing area of the code base, is able to recognise and propose an improvement. Is able to give an estimate and execute on proposal that does not affect deliverables.
    Security:
    - Is aware of company established development processes that are aimed at reducing security related issues. Is able to produce a solution based on the said processes.
    Complexity:
    - Able to tackle all but large issues independently. Large issues with provided clear requirements and design implementation require little effort, issues that are not clearly defined require team assistance.
    - Asks good questions to clarify expectation.
    - Ask for help when an issue is taking longer to implement, works with the team to split issue into further issues.
    "Technical Stewardship":
    - Aware of technical debt and tries, as far as reasonable, to not introduce more of it.
    - Perform thorough reviews within their domain and submit helpful comments.
    Performance:
    - Able to resolve moderately complex performance issues independently.
    - Able to monitor and debug performance issues of moderate complexity and resolve them.
    "Open Source":
    - Encourages participation from those in the community.
  "Leadership Competencies":
    "Growing others":
    - Still largely concentrating on growing themselves.
    - Continually looks for opportunities to grow their technical skills
    - Often works with other team members to grow their own understanding of the team's domain through code reviews, pair programming or mentoring sessions.
    - Participates in code reviews and uses [best practices](https://docs.gitlab.com/ee/development/code_review.html#best-practices) when giving and receiving feedback.
    - Provides additional feedback whenever possible (i.e. 360s, Pulse, etc).
    "Communication":
    - Communicates clearly and effectively, and follows the GitLab [communication guidelines](https://about.gitlab.com/handbook/communication/).
    "Ambiguity":
    - Delivers work given clear requirements within the context of their team.
    - Makes responsible decisions, and evaluates trade-offs.
    - Remains calm under pressure.
    "Business Acumen":
    - Demonstrates knowledge about their features and category and how it fits into their group.
    "Process":
    - Works effectively within established processes, and keeps themselves current as processes change.
    - Seeks to understand how their works integrates with processes across the company

- name: Senior
  "Values Alignment":
    Collaboration:
    - Helps their team succeed. Provides feedback that focuses on the business impact of the behaviour.
    - Engages with the stable-counterparts for their team.
    - Extensive user of GitLab (dogfooding) and regularly makes helpful suggestions for feature proposals.
    Results:
    - Demonstrates ownership of projects that require collaboration with other teams in their section.
    - Creates a sense of urgency to deliver results and encourages bias for action in others.
    Efficiency:
    - Chooses work that enables greater efficiency of others in the team.
    Diversity:
    - Seeks out diverse opinions to improve the quality of the team's work.
    Iteration:
    - Demonstrates understanding of balance between short term gains and long term benefit.
    - Creates proposals for team work that clearly defines the problem, offers alternatives, and proposes a solution.
    - Identifies opportunities for the team to deliver results in a more iterative way.
    Transparency:
    - Holds their team to a higher standard of transparency and encourages others to work in the open.
  "Technical Competencies":
    Quality:
    - It is important that at every level we adhere to the minimum requirement of writing tests with every MR.
    - Expected to uplevel the quality of the code.
    - When working in an existing area, strives to leave it in a better state than before.
    - Improves the state of our test framework and any other dependencies.
    - Work to guide others and improve the performance of the tests.
    - Always looks to improve our test coverage with every MR coded or reviewed.
    Security:
    - In addition to fixing security issues, contribute to the team's understanding of security and how they impact the team's domain.
    - Able to take on issues, come up with an effective approach and deliver a working solution.
    Complexity:
    - Able to take on large issues with vague requirements and design implementation.
    - Asks good questions to clarify expectation.
    - Recognizes when an issue becomes much larger than originally expected and able to create further issues and propose the next simplest implementation to follow.
    "Technical Stewardship":
    - Care for GitLab's code base.
    - Provide helpful reviews across many domains and be mindful of how changes may impact other teams.
    - Be a Maintainer for smaller projects.
    Performance:
    - Able to resolve complex performance issues and mentor others on best practices.
    - Understands how code performs in production on GitLab.com and impact of the architecture and dependencies required for the self-hosted product
    "Open Source":
    - Act as coach for open source contributors.
    - Encourage participation from those in the community.
  "Leadership Competencies":
    "Growing others":
    "Communication":
    - Communicates clearly and effectively, and follows the GitLab [communication guidelines](https://about.gitlab.com/handbook/communication/).
    "Ambiguity":
    - Delivers work given unclear requirements within the context of their team.
    - Makes responsible decisions, and evaluates trade-offs.
    - Maintains calm under pressure.
    "Business Acumen":
    - Demonstrates knowledge about their groups, and how their stage fits into the larger business.
    - Also exhibits an understanding of a number of other team's products
    "Process":
    - Seeks to understand how their work integrates with processes across the company.
    - Looks for opportunities for process improvements within their team and works with others to implement process changes

- name: Staff
  "Values Alignment":
    Collaboration:
    - Help the team succeed and looks for opportunities to help the section to succeed.
    - Identifies situations where collaboration between teams will yield good results.
    Results:
    - Iteratively works towards results on cross-team projects without over-analysis.
    Efficiency:
    - Chooses work that is appropriate to their level and works with the Engineering Manager to assign work to other members of the team.
    Diversity:
    - Directly or indirectly takes part in the hiring process to help ensure that we bring in a wide variety of experience into our teams.
    Iteration:
    - Is able to take a long term goal and turn this into small actionable steps that can be implemented in an iterative way. Identifies and prevents decisions that are not ["two-way door decisions"](/handbook/values/#make-two-way-door-decisions).
    Transparency:
    - Champions a no-blame culture and encourages learning from mistakes.
  "Technical Competencies":
    Quality:
    - A champion for Quality and makes sure it is everyone's responsibility.
    - Mentor others on how to keep the quality bar high.
    - Hold MRs to our high standards of done and work with engineers to improve their tests and prevent future regressions.
    - Takes on initiatives that address performance issues and work with the quality team on any initiatives that would simplify life for the developers and contributors.
    - Champion for tech debt, customer issues, and high severity bugs.
    Security:
    - A champion for security.
    - Work closely with our internal security team, provide guidance to others on the team regarding how to properly address security issues and vulnerability.
    - Expert at using the security features of our product and mentor to others to ensure we adopt our own tools.
    - Provide feedback to the secure team and act as a customer for this stage of the product.
    Complexity:
    - Able to take on complex requirements and decompose them into a proposal of small deliverables.
    - Able to quickly pivot a solution based on feedback or complexity.
    - Mentors and guides others on the team to achieve this goal
    "Technical Stewardship":
    - Be a gate keeper for quality; either as a CE/EE Maintainer or working towards this responsibility.
    - Expected to make this achievement as one of the top goals.
    Performance:
    - Able to resolve complex performance issues and mentor others on best practices.
    - Understands how code in their domain performs in production on GitLab.com and impact of the architecture and dependencies required for the self-hosted product.
    - Able to guide and discuss performance issues from customers and quickly provide solutions that align with product direction.
    - A champion of technical initiatives in this area and works closely with product to help prioritize the work.
    "Open Source":
    - Active participants in open source.
    - Act as coach for open source contributors.
    - Encourage participation from those in the community and contribute bugs and features to upstream dependencies where appropriate.
  "Leadership Competencies":
    "Growing others":
    - Continually works with others in their section to help those individuals grow their understanding of the team's domain and technology.
    - Participates in code reviews and uses best practices (https://docs.gitlab.com/ee/development/code_review.html#best-practices) when giving and receiving feedback.
    - Provides additional feedback whenever possible (i.e. 360s, Pulse, etc).
    Communication:
    - Communicates clearly, and effectively in both written and verbally, both internally, and with customers.
    - Follows the GitLab [communication guidelines](https://about.gitlab.com/handbook/communication/).
    Ambiguity:
    - Delivers work given unclear requirements within the context of their section.
    - Makes responsible decisions, and evaluates tradeoffs.
    - Maintains calm under pressure.
    Business Acumen":
    - Exhibits a deep understanding of all of the products owned by their section, how customers use them, and how they fit in to the larger business.
    Process:
    - Works effectively within established processes, and keeps themselves current as processes change.
    - Looks for opportunities for process improvements for their section and works with others to implement process changes.
